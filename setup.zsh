#!/usr/bin/zsh
########################################################################
#
#           Setup Environment for Travis
#
#   This script sets up symbolic links to the ~/.shell_settings repo to
#   make it easy to work on a new linux machine.
#
########################################################################

SETTINGS_PATH=$HOME/.shell_settings

TMUX_CONF=$HOME/.tmux.conf
VIM_DIR=$HOME/.vim
VIM_RC=$HOME/.vimrc
ZSH_RC=$HOME/.zshrc
OHMYZSH_THEME=$HOME/.oh-my-zsh/themes/travis.zsh-theme
GIT_CONFIG=$HOME/.gitconfig
GIT_USER=$HOME/.gituser
NVIM_CONFIG=$HOME/.config/nvim
TRAVIS_BIN=$HOME/.local/bin
FLAKE8_CONF=$HOME/.flake8
BLACK_CONF=$HOME/.config/black

# Remove the needed symbolic links
echo "Removing symbolic links..."
rm $TMUX_CONF
rm -rf $VIM_DIR
rm $VIM_RC
rm $ZSH_RC
rm $OHMYZSH_THEME
rm $GIT_CONFIG
rm $GIT_USER
rm $NVIM_CONFIG
rm $FLAKE8_CONF
rm $BLACK_CONF
rm $TRAVIS_BIN/tmxfieldsupport
rm $TRAVIS_BIN/tmxdev
rm $TRAVIS_BIN/tmxouter
rm $TRAVIS_BIN/set_host

echo "Creating symbolic links..."
ln -s $SETTINGS_PATH/git/gitconfig $GIT_CONFIG
ln -s $SETTINGS_PATH/git/gituser $GIT_USER
ln -s $SETTINGS_PATH/zsh/themes/travis.zsh-theme $OHMYZSH_THEME
ln -s $SETTINGS_PATH/tmux/tmux.conf $TMUX_CONF
ln -s $SETTINGS_PATH/vim $VIM_DIR
ln -s $SETTINGS_PATH/vim/vimrc $VIM_RC
ln -s $SETTINGS_PATH/zsh/zshrc $ZSH_RC
ln -s $SETTINGS_PATH/nvim/config $NVIM_CONFIG
ln -s $SETTINGS_PATH/python/flake8/flake8.toml $FLAKE8_CONF
ln -s $SETTINGS_PATH/python/black/black.toml $BLACK_CONF

ln -s $SETTINGS_PATH/scripts/tmxfieldsupport $TRAVIS_BIN/tmxfieldsupport
ln -s $SETTINGS_PATH/scripts/tmxdev $TRAVIS_BIN/tmxdev
ln -s $SETTINGS_PATH/scripts/tmxouter $TRAVIS_BIN/tmxouter
ln -s $SETTINGS_PATH/scripts/set_host $TRAVIS_BIN/set_host
