autoload -Uz vcs_info

zstyle ':vcs_info:*' stagedstr '%F{green}●'
zstyle ':vcs_info:*' unstagedstr '%F{yellow}●'
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{11}%r'
zstyle ':vcs_info:*' enable git svn
git_precmd () {
    if [[ -z $(git ls-files --other --exclude-standard 2> /dev/null) ]] {
        zstyle ':vcs_info:*' formats ' (%b%c%u%B%F{green})'
    } else {
        zstyle ':vcs_info:*' formats ' (%b%c%u%B%F{red}●%F{green})'
    }

    vcs_info
}

setopt prompt_subst

function trprompt {

    # Start with a newline
    PS1="
"

    # Change colors for host
    HOST=$(hostname)
    if [[ $HOST == "pascal" ]] ; then
        local hostcolor="$fg[cyan]"
    elif [[ $HOST == "angstrom" ]] ; then
        local hostcolor="$fg[magenta]"
    else
        local hostcolor="$fg[green]"
    fi
    PS1+=" %B${hostcolor}%m  %{$reset_color%}"

    # Add time
    PS1+="%F{23}%D{%H:%M}  %{$reset_color%}"

    # ptag variable. Used by ros chrt.
    if [[ -n $ptag ]]; then
        PS1+="%B%F{13}${ptag} %{$reset_color%}"
    fi

    # Python Virtual Environment variable
    if [[ -n $VIRTUAL_ENV ]]; then
        PS1+="%B%F{13}venv %{$reset_color%}"
    fi

    # Add relative path
    PS1+="%B%F{magenta}%~%{$reset_color%}"
    git_precmd
    PS1+="%B%F{green}${vcs_info_msg_0_}%{$reset_color%}"
    PS1+="
%B%F{magenta}❯ %{$reset_color%}"

}

# PROMPT='
#  %B%{$fg[yellow]%}%m  %{$reset_color%}%F{23}%D{%H:%M}  %B%F{magenta}'%~'%B%F{green}${vcs_info_msg_0_}%{$reset_color%}
# %B%F{magenta}❯ %{$reset_color%}'

# RPROMPT='%B%F{gray}%D{%m/%d %H:%M}%{$reset_color%}'

autoload -U add-zsh-hook
# add-zsh-hook precmd  theme_precmd trprompt
add-zsh-hook precmd trprompt

unset LSCOLORS
