--[[ init.lua ]]
-- Travis Riggs

-- IMPORTS
require('travis/options')        -- Options
require('travis/plugins')        -- Plugins
require('travis/mappings')       -- Keymaps
