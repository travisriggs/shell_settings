-- Bootstrap Packer
local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

-- List of Plugins
return require('packer').startup(function(use)

  use { 'wbthomason/packer.nvim' }

  -- [[ Development ]]
  use { 'kyazdani42/nvim-web-devicons' }
  use { 'numToStr/Comment.nvim',
        config = "require'travis/plugins-config/comment'" }
  use { 'kyazdani42/nvim-tree.lua',
        requires = {'kyazdani42/nvim-web-devicons'},
        config = "require'travis/plugins-config/nvim-tree'" }
  use { 'nvim-telescope/telescope.nvim',
        requires = {'nvim-lua/plenary.nvim'},
        config = "require'travis/plugins-config/telescope'" }
  use { 'lewis6991/gitsigns.nvim', config = "require'travis/plugins-config/gitsigns'" }
  use { "ThePrimeagen/harpoon", config = "require'travis/plugins-config/harpoon'" }
  use { 'j-morano/buffer_manager.nvim',
        requires = {'nvim-lua/plenary.nvim'},
        config = "require'travis/plugins-config/buffer-manager'" }
  -- For some reason, 'config' key does not load buffer-manager configuration
  require("travis/plugins-config/buffer-manager")
  use { 'zakharykaplan/nvim-retrail',
        config = "require'travis/plugins-config/retrail'" }
  use { 'majutsushi/tagbar' }
  -- use { 'tmhedberg/SimpylFold' }

  -- [[ Syntax ]]
  use { "nvim-treesitter/nvim-treesitter",
        run = ":TSUpdate",
        event = "BufEnter",
        after = "nvim-tree.lua",
        config = "require'travis/plugins-config/treesitter'" }
  use { 'kergoth/vim-bitbake' }
  use { "http://gitlab.com/HiPhish/jinja.vim" }

  -- [[ Theme ]]
  use { 'nvim-lualine/lualine.nvim',
        requires = {'kyazdani42/nvim-web-devicons'},
        config = "require'travis/plugins-config/lualine'" }
  -- Highlight cursor jumps
  use { "DanilaMihailov/beacon.nvim" }
  -- Keep NeoSolarized as option for its light background
  use { "overcache/NeoSolarized" }
  use { "tjdevries/colorbuddy.nvim" }
  use { "svrana/neosolarized.nvim", requires = {"tjdevries/colorbuddy.nvim"} }
  -- Cannot get this config to load with 'config' key
  require("travis/plugins-config/neosolarized")

  -- [[ Help ]]
  use { "folke/which-key.nvim", config = "require'travis/plugins-config/whichkey'" }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
