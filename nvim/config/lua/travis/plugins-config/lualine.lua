local lualine = require("lualine")

lualine.setup({
  options = {
    -- theme = 'powerline'
    theme = 'solarized'
  }
})
