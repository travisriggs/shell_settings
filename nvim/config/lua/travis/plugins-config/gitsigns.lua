local gitsigns = require "gitsigns"

gitsigns.setup({
  -- Keymaps
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns

    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    -- Navigation
    map('n', ']h', function()
      if vim.wo.diff then return ']c' end
      vim.schedule(function() gs.next_hunk() end)
      return '<Ignore>'
    end, {expr=true})

    map('n', '[h', function()
      if vim.wo.diff then return '[c' end
      vim.schedule(function() gs.prev_hunk() end)
      return '<Ignore>'
    end, {expr=true})

    -- Actions
    map('n', '<leader>hB', function() gs.blame_line{full=true} end)
    map('n', '<leader>hb', gs.toggle_current_line_blame)
    map('n', '<leader>hd', gs.diffthis)
    map('n', '<leader>hD', function() gs.diffthis('~') end)
  end,

  current_line_blame_opts = {
    delay = 250,
    ignore_whitespace = true,
  },
  preview_config = {
    row = 1, -- Default is 0. Offset down one line so doesn't block view of cursor line
  },
})
