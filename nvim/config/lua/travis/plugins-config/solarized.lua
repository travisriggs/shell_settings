local solarized = require "solarized"

solarized.setup({
  mode = 'dark', -- dark(default)/light
  theme = 'neovim', -- vim(default)/neovim/vscode
  transparent = false, -- false(default)/true
})

vim.cmd 'colorscheme solarized'
