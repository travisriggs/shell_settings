n = require("neosolarized").setup({
  comment_italics = true,
  background_set = true,
  background_color = require("colorbuddy.init").Color.new("custom_bg", "#00252E")
})

n.Group.new('PreProc', n.colors.yellow)
-- n.Group.link('WarningMsg', n.groups.Comment)
n.Group.new("NormalNC", n.colors.base0, n.bg_color)
