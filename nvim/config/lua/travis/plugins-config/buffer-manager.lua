require("buffer_manager").setup({
  line_keys = "1234567890",
  focus_alternate_buffer = false,
  select_menu_item_commands = {
    edit = {
      key = "<CR>",
      command = "edit"
    },
    v = {
      key = "<C-v>",
      command = "vsplit"
    },
    h = {
      key = "<C-h>",
      command = "split"
    }
  },
  width = 0.85,
})
