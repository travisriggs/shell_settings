local function debug_print_tbl(tbl)
  print('{')
  for k, v in pairs(tbl) do
    print('  ', k, ': ', v)
  end
  print('}')
end

local function map(mode, lhs, rhs, opts)
  default_opts = { noremap = true, silent = true }
  opts = opts or {}
  opts = vim.tbl_extend('force', default_opts, opts)
  vim.api.nvim_set_keymap(mode, lhs, rhs, opts)
end

-- Make leader spacebar
vim.g.mapleader = ' '

-- Move between windows.
map('n', '<C-h>', '<C-W>h')
map('n', '<C-j>', '<C-W>j')
map('n', '<C-k>', '<C-W>k')
map('n', '<C-l>', '<C-W>l')

-- Next, prev buffer
map('n', ']b', ':bnext<CR>')
map('n', '[b', ':bprev<CR>')

-- Open Buffer Manager
map('n', '<Leader>b', ':lua require("buffer_manager.ui").toggle_quick_menu()<CR>')

-- Toggle Nvim Tree
map('n', '<Leader>n',  ":NvimTreeToggle<cr>")
map('n', '<Leader>ft', ":NvimTreeFindFile<cr>")

-- Telescope
map('n', '<Leader>ff', ":Telescope find_files<cr>")
map('n', '<Leader>fg', ":Telescope live_grep<cr>")
map('n', '<Leader>fo', ":lua require('telescope.builtin').live_grep({grep_open_files=true})<cr>")
map('n', '<Leader>fb', ":Telescope buffers<cr>")
map('n', '<Leader>fh', ":Telescope help_tags<cr>")
map('n', '<Leader>f/', ":Telescope current_buffer_fuzzy_find<cr>")

-- Harpoon
map("n", "<leader>m", ":lua require('harpoon.mark').add_file()<CR>")
map("n", "<A-m>",     ":lua require('harpoon.ui').toggle_quick_menu()<CR>")
map("n", "<A-j>",     ":lua require('harpoon.ui').nav_next()<CR>")
map("n", "<A-k>",     ":lua require('harpoon.ui').nav_prev()<CR>")
map("n", "<leader>1", ":lua require('harpoon.ui').nav_file(1)<CR>")
map("n", "<leader>2", ":lua require('harpoon.ui').nav_file(2)<CR>")
map("n", "<leader>3", ":lua require('harpoon.ui').nav_file(3)<CR>")
map("n", "<leader>4", ":lua require('harpoon.ui').nav_file(4)<CR>")
map("n", "<leader>5", ":lua require('harpoon.ui').nav_file(5)<CR>")
map("n", "<leader>6", ":lua require('harpoon.ui').nav_file(6)<CR>")
map("n", "<leader>7", ":lua require('harpoon.ui').nav_file(7)<CR>")
map("n", "<leader>8", ":lua require('harpoon.ui').nav_file(8)<CR>")
map("n", "<leader>9", ":lua require('harpoon.ui').nav_file(9)<CR>")
map("n", "<leader>0", ":lua require('harpoon.ui').nav_file(0)<CR>")

-- GitSigns
-- These mappings are defined in plugins-config/gitsigns.lua
