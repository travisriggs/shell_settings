-- Disable netrw, due to race conditions at vim startup for nvim-tree
vim.g.loaded = 1
vim.g.loaded_netrwPlugin = 1

vim.opt.mouse = 'a'            -- Enable use of the mouse for all modes
vim.opt.tabstop = 8
vim.opt.shiftwidth = 4
vim.opt.smarttab = true
vim.opt.expandtab = true
vim.opt.hidden = true          -- Vim is completely unusable without this!
vim.opt.number = true          -- Show line numbers
vim.opt.cursorline = true

-- Neovim Optional Providers
vim.g.python3_host_prog = '/usr/bin/python3'
vim.g.loaded_node_provider = 0  -- Disable perl provider
vim.g.loaded_ruby_provider = 0  -- Disable perl provider
vim.g.loaded_perl_provider = 0  -- Disable perl provider

-- Highlight current line with a black background
vim.cmd('hi CursorLine cterm=NONE ctermbg=black guibg=black')

vim.opt.colorcolumn = '81'

-- New windows open below or to right of current one (why isn't this default?)
vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.shell = '/bin/zsh'
vim.opt.encoding = 'utf-8'
vim.opt.fileencoding = 'utf-8'

-- [[ Theme ]]
-- vim.opt.syntax = "ON"
vim.opt.termguicolors = true
vim.opt.background = 'dark'
-- vim.cmd('colorscheme solarized')
-- vim.cmd('colorscheme NeoSolarized')
vim.cmd('colorscheme neosolarized')

-- vim.wo.wrap = false
-- vim.wo.linebreak = false
-- vim.o.history = 500
-- vim.o.ruler = true          -- Always show a window status line, even when only 1 win
-- vim.o.showcmd = true        -- Show partially typed commands in status area
-- vim.o.incsearch = true      -- Incremental search
-- vim.o.inccommand = 'split'  -- Shows incremental results of substitute cmd in split (Nvim only)
-- vim.o.ignorecase = true     -- the case of normal letters is ignored
-- vim.o.smartcase = true      -- upper case letters in search turn off ignorecase
-- vim.o.wrapscan = true       -- wrap to top of file on search
-- vim.o.showmatch = true      -- When a bracket is inserted, briefly jump to the matching.,s'
-- vim.o.whichwrap = '<,>,[,],b,s'
--
-- vim.o.hlsearch = true
-- vim.o.gdefault = true       -- assume the /g flag on :s substitutions to replace all matches in a line
-- vim.o.modeline = true
-- vim.o.modelines = 5
-- vim.o.showfulltag = true    -- Insert mode completion of tag shows whole funct prototype!
-- vim.o.laststatus = 2        -- Always show a status line, even when only 1 window
-- vim.o.colorcolumn = '81'
-- vim.o.swapfile = false      -- Swap files are so annoying!
-- vim.o.relativenumber = true
-- vim.o.cursorcolumn = true
-- vim.cmd('behave xterm')     -- Mouse drag enters Visual mode instead of Select mode.
--                             -- behave sets several options. See help
-- vim.o.startofline = false   -- Don't gatuitously move cursor to 1st column
-- vim.o.path = vim.o.path .. '**' -- File operations recurse directories
-- vim.o.signcolumn = 'auto:6'
-- vim.o.winminheight = 0
--
--
-- -- Format Options
-- vim.o.formatoptions = vim.o.formatoptions .. 'j' -- Delete comment char when joining commented lines
-- vim.o.formatoptions = vim.o.formatoptions .. 'c' -- Auto-format comments in insert mode
-- -- HOW TO REMOVE? vim.o.formatoptions-=o -- (don't) Insert the comment leader after hitting 'o' etc.
-- vim.o.textwidth = 78
-- vim.o.updatetime = 300      -- Vim's hover timeout. Default 4000. Want this low for more reactive coc plugins
--
-- -- Tighter X server clipboard integration. Of these unnamed and unnamedplus
-- -- are added to the defaults.
-- -- This port from vimscript gives Lua error: vim.o.clipboard = 'unnamed,unnamedplus,autoselect,exclude:cons\|linux'
-- vim.o.clipboard = 'unnamed,unnamedplus'
--
-- vim.o.backspace = 'indent,eol,start' -- allow backspacing over everything in insert mode
--
-- -- make the completion mode behave more like bash -- complete the longest
-- -- string, then list the alternatives in a popup menu.
-- vim.o.wildmode = 'longest:full'
-- vim.o.wildmenu = true
-- vim.o.wildoptions = 'pum,tagfile'  -- pum = Make the wild menu vertical
--
-- -- Fix delay exiting insert mode using Esc. Default was 1000ms
-- -- See https://www.johnhawthorn.com/2012/09/vi-escape-delays/
-- vim.o.timeoutlen = 200
--
-- -- Re-use of existing windows when switching buffers:
-- vim.o.switchbuf = 'useopen,usetab,split'
--
-- -- Be a little bit more Common User Interface-like
-- vim.o.virtualedit = 'onemore,block'
--
-- -- [ C/C++ formatting options ]
-- -- NOTE: Nvim's Lua :append({'valu'}) syntax didn't work for cinoptions. Why?
--
-- -- Indent C++ class declarations & constructor initializations, if they start
-- -- on a new line, by double the shiftwidth
-- vim.o.cinoptions = 'i2s'
--
-- -- Indent a contination line of a long expression double the shiftwidth.
-- vim.o.cinoptions = vim.o.cinoptions .. ',+2s'
--
-- -- This option must be set for the following two to work (W and m)
-- vim.o.cinoptions = vim.o.cinoptions .. ',(0'
--
-- -- Indent line following a line ending with open-parenthesis double shiftwidth
-- -- So arguments to a long function call will not start way over to the left,
-- -- which makes going to a new line pointless!
-- vim.o.cinoptions = vim.o.cinoptions .. ',W2s'
--
-- -- Line up a line starting with a closing parentheses with the first character
-- -- of the line with the matching opening parentheses
-- vim.o.cinoptions = vim.o.cinoptions .. ',m1'
--
-- -- Align with case label instead of the statement after it
-- vim.o.cinoptions = vim.o.cinoptions .. ',l1'

-- Specify some per-filetype options:
vim.cmd([[
  autocmd!
    " hard tabs are special in makefiles, don't expand them
    autocmd FileType make set noexpandtab shiftwidth=8 nowrap
    autocmd FileType c set noexpandtab shiftwidth=8 nowrap
    autocmd FileType xml,cpp set expandtab shiftwidth=2 textwidth=0 colorcolumn=100 nowrap
    autocmd FileType gitcommit set expandtab shiftwidth=4 textwidth=72 colorcolumn=73 wrap
    autocmd FileType md,txt set expandtab shiftwidth=4 textwidth=79 wrap
    autocmd FileType python,sh,perl set expandtab shiftwidth=4 colorcolumn=73,100 textwidth=99 wrap
    autocmd FileType lua set expandtab shiftwidth=2 textwidth=0 colorcolumn=100 nowrap
    autocmd BufRead,BufNewFile Jenkinsfile setf groovy
    au BufRead,BufNewFile *.launch  setfiletype xml
    au BufRead,BufNewFile *.action  setfiletype yaml
    au BufRead,BufNewFile *.msg  setfiletype yaml
    au BufRead,BufNewFile *.srv  setfiletype yaml
    au BufRead,BufNewFile *.sls  setfiletype yaml
    au BufRead,BufNewFile *.cfg  setfiletype python
    au BufRead,BufNewFile *.j2  setfiletype jinja
  augroup END
]])
