#!/usr/bin/zsh
########################################################################
#
#           Start Tmux Session for Local Robot Development
#            by Travis Riggs
#
#   Usage:
#
#     tmxbot <robot_number>
#
########################################################################

BOTNUMBER=$1
ROBOT=bar$BOTNUMBER
SESSION=bar${1: -3}

# Make sure ros chrt is mounted
sudo /rosn/ros-chrt-mount

# Setup some basic commands
SSH_TO_TRUCK="ssh -p 2222 -i $HOME/.ssh/barkey -o CheckHostIP=no -o StrictHostKeyChecking=no bar@$ROBOT"
SSH_TO_PAYLOAD="ssh -p 3222 -i $HOME/.ssh/barkey -o CheckHostIP=no -o StrictHostKeyChecking=no bar@$ROBOT"
TELEOP="rosrun teleop_twist_keyboard teleop_twist_keyboard.py cmd_vel:=/vel_mux_unchecked/teleop_keyboard"
# SET_HISTORY="history -r /home/bar/.common_commands"

# Copy the common robot commands file to the robot
# scp -i $HOME/.ssh/barkey -P 2222 -o StrictHostKeyChecking=no $HOME/.shell_settings/scripts/common_robot_commands bar@$ROBOT:/home/bar/.common_commands

# TMUX
tmux new-session -d -s ${SESSION}

###########################################################
## Create window for local machine
WINDOW_1=pascal
tmux rename-window -t ${SESSION}:1 $WINDOW_1
tmux select-pane -t ${SESSION}:$WINDOW_1.1
tmux send-keys -t ${SESSION}:$WINDOW_1.1 "cd ~" Enter
tmux send-keys -t ${SESSION}:$WINDOW_1.1 "ranger" Enter

###########################################################
## Create window for tools
WINDOW_2=tools
tmux new-window -t ${SESSION} -n $WINDOW_2
tmux send-keys -t ${SESSION}:$WINDOW_2 "sudo -E $HOME/.local/bin/ros zsh" Enter
# Fix weird problem with Ctrl-R to search history in ros chrt
tmux send-keys -t ${SESSION}:$WINDOW_2 "bindkey -v" Enter
tmux send-keys -t ${SESSION}:$WINDOW_2 "bindkey '^R' history-incremental-search-backward" Enter
tmux send-keys -t ${SESSION}:$WINDOW_2 "clear" Enter
tmux send-keys -t ${SESSION}:$WINDOW_2 "source $HOME/src/ros/bartruck1/ros/noetic/devel/setup.zsh" Enter
tmux send-keys -t ${SESSION}:$WINDOW_2 ". barenv $BOTNUMBER" Enter
tmux send-keys -t ${SESSION}:$WINDOW_2 "rviz &"

# tmux split-window -t ${SESSION}:1 -h
# tmux send-keys -t ${SESSION}:pascal.top "cd $BASE_PATH" Enter
# tmux send-keys -t ${SESSION}:pascal.bottom "cd $BASE_PATH" Enter

###########################################################
## Create window for SSH into robot's Truck PC
WINDOW_3=TRUCK-general
tmux new-window -t ${SESSION} -n $WINDOW_3
# tmux rename-window -t ${SESSION}:1 $WINDOW_3
tmux split-window -t ${SESSION} -h
tmux split-window -t ${SESSION}:$WINDOW_3.left -v
tmux split-window -t ${SESSION}:$WINDOW_3.right -v
tmux resize-pane -t ${SESSION}:$WINDOW_3.bottom-right -y 8

# List SystemD Services
tmux select-pane -t ${SESSION}:$WINDOW_3.top-right
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "sudo systemctl list-dependencies snap.toetic.target" Enter

# ROS-aware Shell
tmux select-pane -t ${SESSION}:$WINDOW_3.bottom-left
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter

# ROS-aware Shell
tmux select-pane -t ${SESSION}:$WINDOW_3.bottom-right
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter

# System Check
tmux select-pane -t ${SESSION}:$WINDOW_3.top-left
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "sudo toetic.system-check" # Enter

###########################################################
## Create window and panes for analyzing journal and logs
WINDOW_4=TRUCK-logs
tmux new-window -t ${SESSION} -n $WINDOW_4
tmux split-window -t ${SESSION} -h
tmux split-window -t ${SESSION}:$WINDOW_4.left -v
tmux split-window -t ${SESSION}:$WINDOW_4.right -v

# Make the bottom left pane for the syslog
tmux select-pane -t ${SESSION}:$WINDOW_4.bottom-left
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "sudo less /var/log/syslog" # Enter

# Make the top right pane for ROS bash prompt
tmux select-pane -t ${SESSION}:$WINDOW_4.top-right
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "rostopic list | grep "

# Make the lower right pane for ROS logs
tmux select-pane -t ${SESSION}:$WINDOW_4.bottom-right
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "cd /var/snap/bar-base/common/ros_home/log/latest" Enter

# Make the top left pane for the journal
tmux select-pane -t ${SESSION}:$WINDOW_4.top-left
tmux send-keys -t ${SESSION} $SSH_TO_TRUCK Enter
tmux send-keys -t ${SESSION} "toetic.zsh" Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "sudo journalctl -u snap.toetic.control" # Enter

###########################################################
## Create window for SSH into robot's Payload PC
WINDOW_5=PAYLOAD-general
tmux new-window -t ${SESSION} -n $WINDOW_5
# tmux rename-window -t ${SESSION}:1 $WINDOW_5
tmux split-window -t ${SESSION} -h
tmux split-window -t ${SESSION}:$WINDOW_5.left -v
tmux split-window -t ${SESSION}:$WINDOW_5.right -v
tmux resize-pane -t ${SESSION}:$WINDOW_5.bottom-right -y 8

# List SystemD Services
tmux select-pane -t ${SESSION}:$WINDOW_5.top-right
tmux send-keys -t ${SESSION} $SSH_TO_PAYLOAD Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "systemctl list-dependencies snap.insight-payload.target" Enter

# ROS-aware Shell
tmux select-pane -t ${SESSION}:$WINDOW_5.bottom-left
tmux send-keys -t ${SESSION} $SSH_TO_PAYLOAD Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "insight-payload.zsh" Enter

# ROS-aware Shell
tmux select-pane -t ${SESSION}:$WINDOW_5.bottom-right
tmux send-keys -t ${SESSION} $SSH_TO_PAYLOAD Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "insight-payload.zsh" Enter

# ROS-aware Shell
tmux select-pane -t ${SESSION}:$WINDOW_5.top-left
tmux send-keys -t ${SESSION} $SSH_TO_PAYLOAD Enter
# tmux send-keys -t ${SESSION} $SET_HISTORY Enter
tmux send-keys -t ${SESSION} "insight-payload.zsh" Enter

###########################################################
# Select the truck-general window
tmux select-window -t ${SESSION}:TRUCK-general
tmux select-pane -t ${SESSION}:top-left

# Attach to the session
tmux attach-session -t ${SESSION}
